﻿#include <iostream>
#include <vector>
#include <cstdlib>

using  namespace std;

/*struct Rectangle {
private:
    double width;
    double height;
public:
    Rectangle();
    Rectangle(double w, double h) {
        width = w;
        height = h;
    };

    void SetRectangle(double w, double h) {
        width = w;
        height = h;
    }
    void PrintSides() {
        cout << "Width: " << width << "\n" << "Height: " << height << "\n";
    }
    double TheAreaOfTheRectangle() {
        return width * height;
    }
    double ThePerimeterOfTheRectangle() {
        return ((width + height) * 2);
    }
    bool bIsSquare() {
        if (width == height) {
            cout << "It's a Square\n";
            return true;
        }
        else {
            cout << "It's a Rectangle\n";
            return false;
        }
    }
};

int main() {
    Rectangle Figure(11.72, 24.73);
    Figure.SetRectangle(11.72, 24.73);
    Figure.bIsSquare();
    cout << "\n" << Figure.TheAreaOfTheRectangle() << "\n" << Figure.ThePerimeterOfTheRectangle() << "\n";
    Figure.PrintSides();

    return 0;
}*/


/*
Создать вектор структур Прямоугольник на 10 элементов
Доработайте структуру для рандомного задания размеров
Выведите размеры прямоугольников вектора в формате

vector[0]: 10x10
vector[1]: 10x10

метод отображения размеров нужно реализовать в самой структуре

*/

struct Rectangle {
    int width;
    int height;

    void DisplaySize() {
        cout << "Rectangle: " << width << "x" << height << "\n";
    }
};

int main() {
    vector<Rectangle> rectangles(10);

    for (int i = 0; i < 10; i++)
    {
        rectangles[i].width = rand() % 10 + 1;
        rectangles[i].height = rand() % 10 + 1;
    }

    for (int i = 0; i < 10; i++)
    {
        cout << "vector[" << i << "]: ";
        rectangles[i].DisplaySize();
    }
    return 0;
}